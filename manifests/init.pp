class bitcoind(
  $daemon_args     = '-daemon',
  $systemd_daemon_args = '-server',
  $rpcuser         = '',
  $rpcpassword     = '',
  $ensure          = 'running',
  $maxconnections  = 25,
  $server          = true,
  $proxy           = false,
  $addnodes        = [],
  $paytxfee        = false,
  $txconfirmtarget = 1,
  Boolean $prune   = false,
) {
  if $rpcuser == '' {
    fail('You need to define a login for the JSON-RPC API! Please set bitcoind::rpcuser in your config')
  }

  if $rpcpassword == '' {
    fail('You need to define a password for the JSON-RPC API! Please set bitcoind::rpcpassword in your config')
  }

  package { 'bitcoind':
    ensure => latest,
  }

  group { 'bitcoin':
    ensure    => present,
    allowdupe => false,
  }

  user { 'bitcoin':
    ensure         => present,
    allowdupe      => false,
    shell          => '/bin/bash',
    gid            => bitcoin,
    home           => '/var/lib/bitcoin',
    require        => Group['bitcoin'],
    purge_ssh_keys => true,
  }

  file { '/var/lib/bitcoin':
    ensure  => directory,
    owner   => bitcoin,
    group   => bitcoin,
    mode    => '0750',
    require => User[bitcoin],
  }

  file { '/var/lib/bitcoin/.bitcoin':
    ensure  => directory,
    owner   => bitcoin,
    group   => bitcoin,
    mode    => '0750',
    require => File['/var/lib/bitcoin'],
  }

  file { '/var/lib/bitcoin/.bitcoin/bitcoin.conf':
    ensure  => present,
    owner   => bitcoin,
    group   => bitcoin,
    mode    => '0600',
    content => template('bitcoind/bitcoin.conf.erb'),
    require => File['/var/lib/bitcoin/.bitcoin'],
    notify  => Service[bitcoind],
  }

  file { '/etc/init.d/bitcoind':
    ensure  => absent,
  }

  file { '/etc/systemd/system/bitcoind.service':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => template('bitcoind/bitcoind.service.erb'),
    notify  => Service['bitcoind'],
  }

  $enable = $ensure ? {
    running => true,
    default => false,
  }

  service { 'bitcoind':
    ensure     => $ensure,
    enable     => $enable,
    provider   => systemd,
    hasrestart => true,
    hasstatus  => true,
    require    => [
      Package[bitcoind],
      File[
        '/var/lib/bitcoin/.bitcoin/bitcoin.conf',
        '/etc/systemd/system/bitcoind.service'
      ],
    ],
    subscribe  => File['/etc/systemd/system/bitcoind.service'],
  }

}
